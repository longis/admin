FROM node:14-buster AS builder

# create app directory
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app
# copies package.json and package lock to
# take advantage of layering in image building
COPY package.json package-lock.json /usr/src/app/
RUN npm ci

COPY . /usr/src/app
RUN git submodule update --init --recursive
RUN npm run build


FROM nginx:stable-alpine AS production
COPY --from=builder /usr/src/app/dist/admin /usr/share/nginx/html

EXPOSE 80
