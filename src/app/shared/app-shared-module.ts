import {CUSTOM_ELEMENTS_SCHEMA, ModuleWithProviders, NgModule, OnInit, Optional, SkipSelf} from '@angular/core';
import { CommonModule } from '@angular/common';
import {TranslateModule, TranslateService} from '@ngx-translate/core';
import {environment} from '../../environments/environment';
import {
  ApplicationSettingsConfiguration,
  ErrorModule,
  SIDEBAR_LOCATIONS,
  AppSidebarService
} from '@universis/common';
import {AdvancedFormsModule} from '@universis/forms';
import {RouterModule} from '@angular/router';
import {MostModule} from '@themost/angular';
import {APP_SIDEBAR_LOCATIONS} from '../app.sidebar.locations';

export declare interface ApplicationSettings extends ApplicationSettingsConfiguration {
  useDigitalSignature: boolean;
  title?: string;
  maxExportItems?: number;
}

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    TranslateModule,
    AdvancedFormsModule,
    ErrorModule,
    MostModule
  ],
  declarations: [
  ],
  providers: [
    {
      provide: SIDEBAR_LOCATIONS,
      useValue: APP_SIDEBAR_LOCATIONS
    }
  ],
  exports: [
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
  ]
})
export class AppSharedModule implements OnInit {
  constructor(@Optional() @SkipSelf() parentModule: AppSharedModule,
              private _translateService: TranslateService,
              private _sidebarService: AppSidebarService) {
    this.ngOnInit().catch(err => {
      console.error('An error occurred while loading app shared module');
      console.error(err);
    });
  }

  static forRoot(): ModuleWithProviders {
    return {
      ngModule: AppSharedModule,
      providers: [
      ]
    };
  }

  async ngOnInit() {
    // create promises chain
    const sources = environment.languages.map(async (language) => {
      const translations = await import(`../../assets/i18n/${language}.json`);
      this._translateService.setTranslation(language, translations, true);
    });
    // execute chain
    await Promise.all(sources);
    // load sidebar config
    await this._sidebarService.loadConfig();
  }
}
