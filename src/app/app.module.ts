import {BrowserModule, Title} from '@angular/platform-browser';
import {APP_INITIALIZER, CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA, NgModule, LOCALE_ID} from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { SharedModule, AuthModule, ErrorModule, ConfigurationService, APP_LOCATIONS,
  UserStorageService, LocalUserStorageService } from '@universis/common';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { AppRoutingModule } from './app-routing.module';
import {AppComponent} from './app.component';
import { RouterModule } from '@angular/router';
import { ModalModule } from 'ngx-bootstrap/modal';
import { IndexComponent } from './layouts/index.component';
import { LocationStrategy, HashLocationStrategy, registerLocaleData, CommonModule } from '@angular/common';
import { AngularDataContext, DATA_CONTEXT_CONFIG } from '@themost/angular';
import { AppSidebarModule } from '@coreui/angular';
// LOCALES: import extra locales here
import en from '@angular/common/locales/en';
import el from '@angular/common/locales/el';
import { THIS_APP_LOCATIONS } from './app.locations';

import {AdminSharedModule} from '@universis/ngx-admin';
import {AppSharedModule} from './shared/app-shared-module';
import {TablesModule} from '@universis/ngx-tables';

import {CollapseModule} from 'ngx-bootstrap/collapse';
import { ProgressbarModule } from 'ngx-bootstrap/progressbar';
import {NgPipesModule} from 'ngx-pipes';
import {AdvancedFormsModule} from '@universis/forms';
import {AppConfig} from './formio-app-config';
import {RouterModalModule} from '@universis/common/routing';
import {FormioAppConfig, FormioModule} from 'angular-formio';

@NgModule({
  declarations: [
    AppComponent,
    IndexComponent
  ],
  imports: [
    CommonModule,
    BrowserModule,
    HttpClientModule,
    TranslateModule.forRoot(),
    SharedModule.forRoot(),
    AppSharedModule.forRoot(),
    RouterModule,
    AuthModule.forRoot(),
    FormsModule,
    AppRoutingModule,
    ErrorModule.forRoot(),
    AppSidebarModule,
    ModalModule.forRoot(),
    AdvancedFormsModule.forRoot(),
    TablesModule,
    NgPipesModule,
    CollapseModule,
    FormioModule,
    ProgressbarModule.forRoot(),
    BsDropdownModule.forRoot(),
    AdminSharedModule.forRoot(),
    RouterModalModule
  ],
  providers: [
    Title,
    {
      provide: DATA_CONTEXT_CONFIG, useValue: {
        base: '/',
        options: {
          useMediaTypeExtensions: false,
          useResponseConversion: true
        }
      }
    },
    {
      provide: APP_LOCATIONS,
      useValue: THIS_APP_LOCATIONS
    },
    AngularDataContext,
    {
      provide: APP_INITIALIZER,
      // use APP_INITIALIZER to load application configuration
      useFactory: (configurationService: ConfigurationService) =>
        () => {
          // load application configuration
          return configurationService.load().then( loaded => {
            // register locales
            registerLocaleData(en);
            registerLocaleData(el);
            // return true for APP_INITIALIZER
            return Promise.resolve(true);
          });
        },
      deps: [ ConfigurationService ],
      multi: true
    },
    {
      provide: LOCALE_ID,
      useFactory: (configurationService: ConfigurationService) => {
        return configurationService.currentLocale;
      },
      deps: [ConfigurationService]
    },
    // use hash location stategy
    // https://angular.io/api/common/HashLocationStrategy
    {
      provide: LocationStrategy,
      useClass: HashLocationStrategy
    },
    {
      provide: UserStorageService,
      useClass: LocalUserStorageService
    },
    {
      provide: FormioAppConfig,
      useValue: AppConfig
    },
  ],
  bootstrap: [ AppComponent ],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA ]
})
export class AppModule {

}
